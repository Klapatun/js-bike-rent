'use strict';

function init() {
    /*Устанавливаю дополнительные стили для фона*/
    document.body.classList.add('index-background')
    const bg = document.getElementsByClassName('layout')[0];
    bg.classList.add('index__layout-bg');

    /*Устанавливаю событие кнопкам*/
    const btn = document.getElementsByClassName('index__content__block_btn')[0].children;
    btn[0].addEventListener('click', ()=>{
        document.location.href += 'catalog';
    });
    btn[1].addEventListener('click', ()=>{
        document.location.href += 'map';
    });
}

document.addEventListener('DOMContentLoaded', init);