'use strict';


for (const iterator of document.getElementsByClassName('bikeBlock')) {
    if(iterator.childElementCount > 1) {
        const btn = iterator.children.item(1).children.item(3);
        btn.addEventListener('click', ()=>{
            const parent = iterator;
    
            const options = {
                method: 'DELETE',
            };
    
            fetch(`/api/order/${btn.dataset.orderId}`, options)
            .then(()=>{
                parent.remove();
            }).catch((error)=>{
                console.error(error);
            });
    
        });
    }
}

cardBlock__btn.addEventListener('click', ()=>{
    location.href = '/card-requisites';
});
