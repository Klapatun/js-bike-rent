'use strict';

function goToCatalog(shop_id) {
    location.href = `/catalog/${shop_id}`;
}

function addBaloons(objArr, myMap) {
    console.log(objArr);
    objArr.forEach(element => {
        const balloon = ymaps.templateLayoutFactory.createClass(
            `<div class="baloon_map">
                <p>Пункт по адресу ${element.address}</p>
                <button class="btn non_absilute" onclick="goToCatalog('${element._id}')">Выбрать велосипед</button>
            </div>`
        );
        const markGeoObj = new ymaps.Placemark(element.coordinates, {address: element.address, id: element._id}, {balloonContentLayout:balloon});
        myMap.geoObjects.add(markGeoObj);
    });
}


function init() {
    const myMap = new ymaps.Map('map', {
        center: [55.016113, 82.912095],
        zoom: 12
    });

    api.getPointers()
    .then((objArr)=>{
        addBaloons(objArr, myMap);
    })
    .catch((error)=>{
        console.error(error);
    });
}


ymaps.ready(init);
