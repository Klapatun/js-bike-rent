'use strict';

let everythingOk = true;

function init() {

    hideError(numberCardNotValid);
    numberCard.placeholder = 'xxxx xxxx xxxx xxxx' ;
    numberCard.addEventListener('blur', cardNumberValidate);
    numberCard.addEventListener('focus', ()=>{
        hideError(numberCardNotValid);
    });

    hideError(validityNotValid);
    validity.placeholder = 'xx/xx' ;
    validity.addEventListener('blur', cardDateValidation);
    validity.addEventListener('focus', ()=>{
        hideError(validityNotValid);
    });

    hideError(cvvCodeNotValid);
    cvvCode.placeholder = 'xxx' ;
    cvvCode.addEventListener('blur', cvvValidate);
    cvvCode.addEventListener('focus', ()=>{
        hideError(cvvCodeNotValid);
    });

}

function cardNumberValidate(cardNumberValue) {
    let cardNumber = cardNumberValue.target.value.replace(/\s/g, '');

    // console.log(cardNumberValue);

    if(cardNumber === '') {
        return;
    }

    /*Проверка на кол-во символов*/
    if(cardNumber.length === 16) {
        cardNumber = +cardNumber;

        /*Проверяем, это цифры или нет*/
        if(Number.isInteger(cardNumber)) {
            /*Форматирование по маске 0000 0000 0000 0000*/
            cardNumber = cardNumber.toString();
            cardNumberValue.target.value = `${cardNumber.slice(0, 4)} ${cardNumber.slice(4, 8)} ${cardNumber.slice(8, 12)} ${cardNumber.slice(12, 16)}`;
        }
        else {
            renderError('В поле должны быть только цифры', numberCardNotValid);
        }
    }
    else {
        renderError('Неверная длина номера', numberCardNotValid);
    }
}

function cardDateValidation(cardDateValue) {
    let cardDate = cardDateValue.target.value;

    if(cardDate === '') {
        return;
    }

    if(!cardDate.includes('/')) {
        renderError('Неверный формат: отсутствует "/"', validityNotValid);
        return;
    }
    
    let [numMonth, numYear] = cardDate.split('/');
    
    numMonth = numMonth.trim();
    numYear = numYear.trim();

    if(numMonth <= 0 || numYear <= 0) {
        renderError('Неверный формат: месяц и год - положительные числа', validityNotValid);
        return;
    }

    if(numMonth.length > 2 || numYear.length > 2) {
        renderError('Неверный формат: месяц и год - двузначные числа', validityNotValid);
        return;
    }

    /*Проверяем, являются ли введенные данные цифрами*/
    numMonth = +numMonth;
    numYear = +numYear;

    if(!Number.isInteger(numMonth) || !Number.isInteger(numYear)) {
        renderError('Неверный формат: используйте цифры', validityNotValid);
        return;
    }

    if(numMonth < 0 || numMonth > 12) {
        renderError('Неверный формат: такого месяца не существует', validityNotValid);
        return;
    }


    /*Проверяем, действует ли еще карта*/
    let currentYear = new Date().getFullYear();
    currentYear %= 100;

    if(numYear < currentYear) {
        console.log(numYear, currentYear, typeof numYear, typeof currentYear);
        renderError('Неверный формат: карта просрочена', validityNotValid);
        return;
    }
    else if(numYear ===  currentYear) {
        if(numMonth < new Date().getMonth()+1) {
            console.log(2);
            renderError('Неверный формат: карта просрочена', validityNotValid);
            return;
        }
    }

    /*Вообще возможно, что эта карта выпущена?*/
    if(numYear-currentYear > 5) {
        renderError('Неверный формат: до этого года карточки не выдают', validityNotValid);
        return;
    }

    cardDateValue.target.value = `${numMonth}/${numYear}`
}

function cvvValidate(cardCvvValue) {
    let cardDate = cardCvvValue.target.value;

    if(cardDate === '') {
        return;
    }

    if(cardDate.length === 3) {
        cardDate = +cardDate;
        if(!Number.isInteger(cardDate)) {
            renderError('В поле должны быть только цифры', cvvCodeNotValid);
        }
    }
    else {
        renderError('Неверная длина CVV', cvvCodeNotValid);
    }
}

function renderError(error, tooltipId) {
    everythingOk = false;
    tooltipId.textContent = error;
    tooltipId.classList.remove('hidden');
}

function hideError(tooltipId) {
    everythingOk = true;
    tooltipId.classList.add('hidden');
}

function submitValid(event) {

    if(numberCard.value === '') {
        renderError('Заполните поле', numberCardNotValid);
        // everythingOk = false;
    }

    if(validity.value === ''){
        renderError('Заполните поле', validityNotValid);
        // everythingOk = false;
    }

    if(cvvCode.value === '') {
        renderError('Заполните поле', cvvCodeNotValid);
        // everythingOk = false;
    }

    if(everythingOk) {
        console.log(everythingOk);
        const options = {
            method: 'PUT',
            headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                date: validity.value, 
                number: numberCard.value,
                cvv: +cvvCode.value.replace(/\s/g, '')
            }),
            credentials: 'include'

        };

        fetch('/api/card-requisites', options)
        .then((jsonObj)=>{
            if(jsonObj.status > 399) {
                Promise.reject(`Error ${jsonObj.status}, ${jsonObj.statusText}`);
            }
            
            /*Проверка url*/
            const pathBack = location.href.split('card-requisites');
            if(pathBack[1] !== '') {
                const bikeId = pathBack[1].split('%2F').pop();
                location.href = `/order/${bikeId}`
            }
            else {
                location.href = '/lk'
            }

        })
        .catch((error)=>{
            console.log(error);
        });
    }

    event.preventDefault()
}

document.addEventListener('DOMContentLoaded', init)
btnSubmit.addEventListener('click', submitValid);
