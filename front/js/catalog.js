'use strict';

function init() {
  let numPage = 1

  /*Если выбор пал на "Все пункты", устанавливаем этому элементу класс active*/
  if(document.URL.split('/').pop() === 'catalog'){
    all_addresses.classList.add('active');
  }

  // вызови функцию loadCatalog для загрузки первой страницы каталога
  loadCatalog(numPage++);
  // Реализуй и установи обработчик нажатия на кнопку "Загрузить еще"
  const btn = loadMore.children[0];
  btn.addEventListener('click', ()=>{

    disableButtonLoadMore();
    loadCatalog(numPage++);
    enableButtonLoadMore();

  });
}

function getBikes(pointId, page) {
  return fetch(`/api/catalog/${pointId}?page=${page}`)
  .then((event)=>{
    return event.json();
  })
  .then((jsonObj)=>{
    return jsonObj;
  })
  .catch((error)=>{
    console.log(error);
  });
}

async function loadCatalog(page) {
  // Здесь необходимо сделать загрузку каталога (api.getBikes)
  // и передать полученные данные в функции appendCatalog и showButtonLoadMore
  const pointId = getPointId();
  
  // const bikes = await api.getBikes(pointId, page);
  const bikes = await getBikes(pointId, page);
  console.log(bikes);

  appendCatalog(bikes.bikesList);
  showButtonLoadMore(bikes.hasMore);
}

function appendCatalog(items) {
  // отрисуй велосипеды из items в блоке <div id="bikeList">
  const bikeWindow = document.getElementById('bikeList');
  items.forEach((bike)=>{
    /*Добавил сам div*/
    const block = document.createElement('div');
    block.classList.add('bikeList__bike');

    /*Вставляется картинка*/
    const img = document.createElement('img');
    img.src = `../images/${bike.img}`;
    const img_div = document.createElement('div');
    img_div.classList.add('bikeList__bike__img');
    img_div.appendChild(img);
    block.appendChild(img_div);

    /*Вставляется наименование товара*/
    const name = document.createElement('p');
    name.classList.add('bikeList__bike__name');
    name.innerText = bike.name;
    block.appendChild(name);

    /*Вставляется цена товара*/
    const cost = document.createElement('p');
    cost.classList.add('bikeList__bike__cost');
    cost.innerText = `Стоимость за час - ${bike.cost*60} ₽`;
    block.appendChild(cost);

    /*Вставляется кнопка*/
    const btn = document.createElement('button');
    btn.textContent = 'Арендовать';
    btn.classList.add('btn');
    btn.addEventListener('click', ()=>{location.href = `/order/${bike._id}`});
    block.appendChild(btn);
    // const btn = document.createElement('a');
    // btn.textContent = 'Арендовать';
    // btn.href = `/order/${bike._id}`
    // block.appendChild(btn);

    bikeWindow.appendChild(block);
  });
}

function showButtonLoadMore(hasMore) {
  // если hasMore == true, то показывай кнопку #loadMore
  // иначе скрывай
  if(hasMore)
    document.getElementById('loadMore').classList.remove('hidden');
  else
  document.getElementById('loadMore').classList.add('hidden');
}

function disableButtonLoadMore() {
  // заблокируй кнопку "загрузить еще"
  const btn = document.getElementById('loadMore').children[0];
  btn.disabled = true; 
}

function enableButtonLoadMore() {
  // разблокируй кнопку "загрузить еще"
  const btn = document.getElementById('loadMore').children[0];
  btn.disabled = false;
}

function getPointId() {
  // сделай определение id выбранного пункта проката
  const pointId = document.URL.split('/').pop();
  return pointId === 'catalog' ? '' : pointId;
}


document.addEventListener('DOMContentLoaded', init)
